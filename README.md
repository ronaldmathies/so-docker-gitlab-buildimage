## 1. What is so-docker-gitlab-buildimage

This image is used for building projects with GitLab CI runners. The image
consists of the Alpine Linux distribution along with Java 8 update 92 and the
Docker CLI.

## 2. Using so-docker-gitlab-buildimage

To use this image with a runner you must first make sure that you have this
image on the docker environment that has the runners.

To achieve this execute the following two commands:

`docker login registry.gitlab.com`

Use your GitLab account as the credentials to login.

`docker pull registry.gitlab.com/ronaldmathies/so-docker-gitlab-buildimage:latest`

This will install the image in your local Docker image cache.

## 3. Configuring a .gitlab-ci.yml

To use this image in a GitLab CI pipeline you must specify this image in the 
following manner:

`
build_project:
  image: registry.gitlab.com/ronaldmathies/so-docker-gitlab-buildimage:latest
`